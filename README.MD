# TASK MANGER

## DEVELOPER INFO

* **Name**: Alexej Malyugin
* **e-mail**: amalyugin@t1-consulting.ru

## SOFTWARE

* **OS**: Windows 10
* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: Core i5-1135G7
* **RAM**: 16 GB
* **SSD**: 512 GB

## PROGRAM RUN

```console
java -jar ./tm.jar
```